//
//  MeteorsNetworking.swift
//  nasa
//
//  Created by Thiago Racca IE on 13/02/2019.
//  Copyright © 2019 Thiago Racca IE. All rights reserved.
//

import Foundation

class MeteorsNetworking {
    
    

func loadMeteorsCall(completion: @escaping (_ result: Bool) -> Void){
    
    let urlString = "https://data.nasa.gov/resource/y77d-th95.json"
    let method : String = "GET"
    
    var request = URLRequest(url: URL(string: urlString)!, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 30.0)
    request.httpMethod = method
    
        let task = URLSession.shared.dataTask(with: request) {
            data, response, error in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                completion(false)
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                completion(false)
                return
                
            }
            
            
            if  let responseString = String(data: data, encoding: .utf8){
       
                
                if let meteors = self.decodeMeteorCall(jsonString: responseString) {
           
                    
                    MeteorPersistence().addOrUpdateMeteors(meteors: meteors) {
                        result in
                        if result {
                            completion(true)
                        }
                        else{
                            completion(false)
                        }
                    }
                    
                }
                else{
                 completion(false)
                }
            }
            else{
                completion(false)
                return
            }
            
            
        }
        task.resume()
 
}

func decodeMeteorCall(jsonString: String) -> [Meteor]?{
    do {
        guard let data: Data = jsonString.data(using: .utf8) else {
            print("bad json - do some recovery \n on call decodeMeteorCall")
            return nil
        }
        let meteors :  [Meteor] = try JSONDecoder().decode([Meteor].self, from: data)
        
        print("meteors \(meteors)")
        return meteors
    }
    catch let error{
        print("bad json - do some recovery \n on call decodeMeteorCall \n error -> \(error)")
        return nil
    }
    
}

}

