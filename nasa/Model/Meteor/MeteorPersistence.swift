//
//  MeteorPersistence.swift
//  nasa
//
//  Created by Thiago Racca IE on 13/02/2019.
//  Copyright © 2019 Thiago Racca IE. All rights reserved.
//

import GRDB


import Foundation




class MeteorPersistence {
    
    // Simple database connection
    var dbQueue : DatabaseQueue?
    
    // Enhanced multithreading based on SQLite's WAL mode
    var dbPool : DatabasePool?
    
    init(){
        let dbmanager = dbManager.shared
        self.dbQueue = dbmanager.dbQueue
        self.dbPool = dbmanager.dbPool
        
    }
    
    
    func getMeteors(_ completion: @escaping (_ result: [Meteor]) -> Void){
        guard self.dbQueue != nil  else { print("dbQueue not found for addMeteors functions"); return}
        do {
            try self.dbQueue!.inTransaction { db in
                let meteorsFoundDatabase = try Row
                    .fetchAll(db, """
                                 SELECT
                                    meteors.id_nasa,
                                    meteors.mass,
                                    meteors.name,
                                    meteors.fell,
                                    meteors.recclass,
                                    meteors.nametype,
                                    meteors.reclat,
                                    meteors.reclong,
                                    meteors.year,
                                    meteor_geolocations.type,
                                    meteor_geolocations.coordinates_0,
                                    meteor_geolocations.coordinates_1
                                    FROM
                                        meteors
                                    INNER JOIN
                                        meteor_geolocations
                                      ON
                                       meteors.meteor_geolocation_id
                                            =
                                       meteor_geolocations.id
                                    WHERE
                                        year != ''
                                    AND
                                        DATETIME(strftime("%s", meteors.year), 'unixepoch')
                                            >
                                        DATETIME(strftime("%s","2010-12-31 23:59:59"), 'unixepoch')
                                    ORDER BY CAST(meteors.mass AS Int) DESC
                                 """)
                
                var meteorsFound : [Meteor] = []
                for row in meteorsFoundDatabase {
                    if  let rowIdDV = row[0]?.databaseValue,
                        let rowMassDV = row[1]?.databaseValue,
                        let rowNameDV = row[2]?.databaseValue,
                        let rowFellDV = row[3]?.databaseValue,
                        let rowRecclassDV = row[4]?.databaseValue,
                        let rowNametypeDV = row[5]?.databaseValue,
                        let rowReclatDV = row[6]?.databaseValue,
                        let rowReclongDV = row[7]?.databaseValue,
                        let rowyearDV = row[8]?.databaseValue,
                        let rowTypeDV = row[9]?.databaseValue,
                        let rowcoordinates0DV = row[10]?.databaseValue,
                        let rowcoordinates1DV = row[11]?.databaseValue
                    {
                        
                
                        
                        let meteorGeolocation = MeteorGeolocation(
                            type: "\(rowTypeDV)".replacingOccurrences(of: "\"", with: ""),
                            coordinates: [
                                Double("\(rowcoordinates0DV)") ?? 0.0,
                                Double("\(rowcoordinates1DV)") ?? 0.0
                            ])
                        
                        let meteor = Meteor(id: "\(rowIdDV)".replacingOccurrences(of: "\"", with: ""),
                                            mass: "\(rowMassDV)".replacingOccurrences(of: "\"", with: ""),
                                            name: "\(rowNameDV)".replacingOccurrences(of: "\"", with: ""),
                                            fell:"\(rowFellDV)".replacingOccurrences(of: "\"", with: ""),
                                            recclass: "\(rowRecclassDV)".replacingOccurrences(of: "\"", with: ""),
                                            nametype: "\(rowNametypeDV)".replacingOccurrences(of: "\"", with: ""),
                                            reclat: "\(rowReclatDV)".replacingOccurrences(of: "\"", with: ""),
                                            reclong: "\(rowReclongDV)".replacingOccurrences(of: "\"", with: ""),
                                            year: "\(rowyearDV)".replacingOccurrences(of: "\"", with: ""),
                                            geolocation: meteorGeolocation)
                        
                        meteorsFound.append(meteor)
                    
                        
                    }
                }
                db.afterNextTransactionCommit(){
                    db in
                    let when = DispatchTime.now() + 0.1
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        completion(meteorsFound)
                    }
                }
                
                return .commit
            }
    
        }
        catch let error {
            print("Error \(error)")
            completion([])
        }
    }
    
    func countMeteors(_ completion: @escaping (_ result: Int?) -> Void){
        guard self.dbQueue != nil  else { print("dbQueue not found for addMeteors functions"); return}
        do {
            try self.dbQueue!.inTransaction { db in
                let idsCount = try Int.fetchOne(db, "SELECT COUNT(*) FROM meteors")
                db.afterNextTransactionCommit(){
                    db in
                    let when = DispatchTime.now() + 0.1
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        completion(idsCount)
                    }
                }
                
                return .commit
            }
        }
        catch let error {
            print("Error \(error)")
            completion(nil)
        }
    }
    
    
    func getMeteor(name: String) -> Meteor?{
        return nil
    }
    
    func getMeteor(id: Int) -> Meteor?{
        return nil
    }
    
    func getNasaIdFromDatabase(_ completion: @escaping (_ result: Bool, _ Ids : [Int]?, _ IdsNasa: [String]?) -> Void){
        guard self.dbQueue != nil  else { print("dbQueue not found for addMeteors functions"); return}
        do {
            try self.dbQueue!.inTransaction { db in
                var idsNasaResult : [String] = []
                var idsResult : [Int] = []
                let idsNasa = try Row
                    .fetchAll(db, "SELECT id,id_nasa FROM meteors")
                for row in idsNasa {
                    if let rowIdDV = row[0]?.databaseValue,
                       let rowIdNasaDV = row[1]?.databaseValue,
                       let rowId = Int("\(rowIdDV)")
                       {
                        let rowNasaId = "\(rowIdNasaDV)".replacingOccurrences(of: "\"", with: "")
                        idsNasaResult.append(rowNasaId)
                        idsResult.append(rowId)
                       }
                }
                db.afterNextTransactionCommit(){
                    db in
                    let when = DispatchTime.now() + 0.1
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        completion(true,idsResult, idsNasaResult)
                    }
                }
       
                return .commit
            }
        }
        catch let error {
            print("Error \(error)")
            completion(false,nil, nil)
        }
    }
    
    func addOrUpdateMeteors(meteors:[Meteor],_ completion: @escaping (_ result: Bool) -> Void) {
        self.addMeteors(meteors: meteors) {
            result in
        }

        
        self.getNasaIdFromDatabase() {
            result, idsResult, idsNasaResult  in
            guard  let idsNasa = idsNasaResult else {
                print("Error with the function getNasaIdFromDatabase")
                return
            }





            //filters Ids to be Inserted
            let insertMeteors = meteors.filter({
                return !idsNasa.contains($0.id)
            })

            //filter Ids to updated
            let updateMeteors = meteors.filter({
                return idsNasa.contains($0.id)
            })


            self.addMeteors(meteors: insertMeteors) {
                result in
                
                if result {
                    
                    self.updateMeteors(meteors: updateMeteors, idsNasa: idsNasa) {
                        result in
                        if result {
                            completion(true)
                        }
                    }
                }
            }

          


        }
    }
    
    func addMeteors(meteors:[Meteor],_ completion: @escaping (_ result: Bool) -> Void){
        guard self.dbQueue != nil else { print("dbQueue not found for addMeteors functions"); return}
        var sqlStringInsert : String = ""
        var sqlValuesInsert : [Any] = []
        
        for meteor in meteors {
            guard let geolocation =  meteor.geolocation,
                geolocation.coordinates.count >= 2 else {
                    continue
            }
            
            sqlStringInsert += """
            INSERT INTO meteor_geolocations (
            type,
            coordinates_0,
            coordinates_1
            ) VALUES (?, ?, ?);
            INSERT INTO meteors (
                id_nasa,
                mass,
                name,
                fell,
                recclass,
                nametype,
                reclat,
                reclong,
                year,
                meteor_geolocation_id
            ) VALUES (?, ?, ?, ?, ?, ?, ? ,? , ?, (SELECT last_insert_rowid()));
            """
            
            //geolocation
            sqlValuesInsert.append(geolocation.type)
            sqlValuesInsert.append(geolocation.coordinates[0])
            sqlValuesInsert.append(geolocation.coordinates[1])
            
            sqlValuesInsert.append(meteor.id)
            sqlValuesInsert.append(meteor.mass ?? "")
            sqlValuesInsert.append(meteor.name)
            sqlValuesInsert.append(meteor.fell ?? "")
            sqlValuesInsert.append(meteor.recclass ?? "")
            sqlValuesInsert.append(meteor.nametype ?? "")
            sqlValuesInsert.append(meteor.reclat ?? "")
            sqlValuesInsert.append(meteor.reclong ?? "")
            sqlValuesInsert.append(meteor.year ?? "")
        }
        
        guard let statmentArguments : StatementArguments = StatementArguments(sqlValuesInsert) else {
            print("statment arguments error for addMeteors functions")
            return
        }
        
        guard sqlStringInsert != "" &&
              sqlValuesInsert.count > 0 else {
                completion(true)
            print("no values to add on the fuction addMeteors")
            return
        }
        
        
        do {
            try self.dbQueue!.inTransaction { db in
                try db.execute(sqlStringInsert, arguments:statmentArguments)
                db.afterNextTransactionCommit() {
                    db in
                    let when = DispatchTime.now() + 0.1
                    DispatchQueue.main.asyncAfter(deadline: when) {

                        completion(true)
                    }
                }
                return .commit
            }
        }
        catch let error {
            print("Error \(error)")
            completion(false)
        }
        
        
    }
    
    func updateMeteors(meteors:[Meteor], idsNasa: [String],_ completion: @escaping (_ result: Bool) -> Void){
        guard  self.dbQueue != nil else { print("dbQueue not found for addMeteors functions"); return}
        var sqlStringUpdate : String = ""
        var sqlValuesUpdate : [Any] = []
        
        for (index,meteor) in meteors.enumerated() {
            guard let geolocation =  meteor.geolocation,
                geolocation.coordinates.count >= 2 else {
                    print("geolocation two coordinates not found for nasa_id \(meteor.id) ")
                    continue
            }
            
            sqlStringUpdate += """
            
            UPDATE meteor_geolocations SET
            type = ?,
            coordinates_0 = ?,
            coordinates_1 = ?
            WHERE id = (SELECT meteor_geolocation_id FROM meteors WHERE id_nasa = ? LIMIT 1);
            
            
            UPDATE meteors SET
            mass = ?,
            name = ?,
            fell = ?,
            recclass = ?,
            nametype = ?,
            reclat = ?,
            reclong = ?,
            year = ?
            WHERE
            id_nasa = ?
            ;
            """
            
            //geolocation
            sqlValuesUpdate.append(geolocation.type)
            sqlValuesUpdate.append(geolocation.coordinates[0])
            sqlValuesUpdate.append(geolocation.coordinates[1])
            sqlValuesUpdate.append(idsNasa[index])
        
            
            sqlValuesUpdate.append(meteor.mass ?? "")
            sqlValuesUpdate.append(meteor.name)
            sqlValuesUpdate.append(meteor.fell ?? "")
            sqlValuesUpdate.append(meteor.recclass ?? "")
            sqlValuesUpdate.append(meteor.nametype ?? "")
            sqlValuesUpdate.append(meteor.reclat ?? "")
            sqlValuesUpdate.append(meteor.reclong ?? "")
            sqlValuesUpdate.append(meteor.year ?? "")
            sqlValuesUpdate.append(idsNasa[index])
        }
        
        guard let statmentArguments : StatementArguments = StatementArguments(sqlValuesUpdate) else {
            print("statment arguments error for addMeteors functions")
            return
        }
        
        guard sqlStringUpdate != "" &&
            sqlValuesUpdate.count > 0 else {
                completion(true)
                print("no values to update on the fuction  updateMeteors")
                return
        }
        
        
        do {
            try self.dbQueue!.inTransaction { db in
                try db.execute(sqlStringUpdate, arguments:statmentArguments)
                db.afterNextTransactionCommit() {
                    db in
                    let when = DispatchTime.now() + 0.1
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        completion(true)
                    }
                }
                return .commit
            }
        }
        catch let error {
            print("Error \(error)")
            completion(false)
        }
        
 
    }
    

    

}
