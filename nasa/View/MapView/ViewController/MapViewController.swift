//
//  MapViewController.swift
//  nasa
//
//  Created by Thiago Racca IE on 17/02/2019.
//  Copyright © 2019 Thiago Racca IE. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation



class MapViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    
    var model = MapViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        

        if let meteor = self.model.meteor,
            let recLatString = meteor.reclat,
            let recLongString = meteor.reclong,
            let recLatDouble = Double(recLatString),
            let recLongDouble = Double(recLongString){

            let annotation = MKPointAnnotation()
            let lat = CLLocationDegrees(recLatDouble)
            let long = CLLocationDegrees(recLongDouble)
            let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
            annotation.coordinate = coordinate
            mapView.addAnnotation(annotation)
            self.zoomToLocation(location: coordinate)
            
            
        }
    }
    
    func zoomToLocation(location : CLLocationCoordinate2D,latitudinalMeters:CLLocationDistance = 10000000,longitudinalMeters:CLLocationDistance = 10000000)
    {
        let region = MKCoordinateRegion(center: location, latitudinalMeters: latitudinalMeters, longitudinalMeters: longitudinalMeters)
        mapView.setRegion(region, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
