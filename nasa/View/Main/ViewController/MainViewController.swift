//
//  ViewController.swift
//  nasa
//
//  Created by Thiago Racca IE on 12/02/2019.
//  Copyright © 2019 Thiago Racca IE. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, MainViewModelDelate {
 
    
    let model = MainViewModel()

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        model.delegate = self
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = true
        
        self.model.getStartLoading() {
            result in
            if result  {
                self.performSegue(withIdentifier: "showList", sender: self)
            }
            else{
                self.model.loadData()
                self.showLoading()
            }
            
        }
        
    }
    
    func showLoading(){
        self.activityIndicator.isHidden = false
        
    }
    
    func loadingFinished() {

        self.performSegue(withIdentifier: "showList", sender: self)
    }


}

