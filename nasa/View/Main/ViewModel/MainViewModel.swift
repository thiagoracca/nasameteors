//
//  MainViewModel.swift
//  nasa
//
//  Created by Thiago Racca IE on 17/02/2019.
//  Copyright © 2019 Thiago Racca IE. All rights reserved.
//

import Foundation

class MainViewModel {
    
    var delegate : MainViewModelDelate?
    
    func loadData(){
        MeteorsNetworking().loadMeteorsCall(completion: {
            result in
            if result,
               let delegate = self.delegate{
                delegate.loadingFinished()
            }
        })
        
       
        
    }
    
    func getStartLoading(_ completion: @escaping (_ result: Bool) -> Void){
        MeteorPersistence().countMeteors() {
            result in
            if let count = result {
                if count > 0 {
                    completion(true)
                }
                else{
                    completion(false)
                }
            }
        }
    }
    
}
